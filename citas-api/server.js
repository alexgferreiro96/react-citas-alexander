const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const datosCitas = require("./datos-citas.json");
const app = express();
app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use("/", express.static('public'));

app.get("/api/citas", (req, res) => {
    const numCitas = datosCitas.length;
    const numAleatorio = getRndInteger(0,numCitas-1);
    res.json(datosCitas[numAleatorio]);
});

function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
  }

  const port = process.env.PORT || 3000;
  app.listen(port, () => console.log("Listening on port " + port));
