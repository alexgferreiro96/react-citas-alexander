const api_url = 'https://heroku-citas-alexander.herokuapp.com//';

export default class Controller {

    
    static getOne = () => {
        return new Promise(
            (resuelve, falla) => {
               fetch(api_url+"api/citas")
                   .then(data => data.json())
                   .then(datos => {
                        resuelve(datos);
                   })
                   .catch(err => {
                       falla(err);
                   });
           });
    }



}

