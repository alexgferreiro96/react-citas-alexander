import React from 'react';
import { Col, Container, Row } from 'reactstrap';
const Cita = (props) => {
    return(
        <Container className="mt-2">
            <Row className="d-flex justify-content-center border rounded">
                <Col sm={12} lg={4} className="order-2 order-lg-1">
                    <img src={props.datosCita.urlImg} alt={props.datosCita.autor} />
                </Col>
                <Col sm={12} lg={3} className="text-center d-flex align-items-center order-1 order-lg-2">
                    <p>{props.datosCita.texto}<br/>-{props.datosCita.autor}</p>
                </Col>
            </Row>
        </Container>
    )
}

export default Cita;