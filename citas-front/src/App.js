import { useEffect, useState } from 'react';
import './App.css';
import Cita from './Cita';
import Controller from './CitaController';
import Loader from 'react-loader-spinner';
const App = () => {
  const [cita, setCita] = useState(null);
  useEffect(()=>{
    Controller.getOne()
    .then(datos => setCita(datos));
  },[]);

  return (
    <div className="App">
      {cita === null ? <Loader type="Hearts" color="#00BFFF" height={80} width={80} /> : <Cita datosCita={cita} />}
    </div>
  );
}

export default App;
